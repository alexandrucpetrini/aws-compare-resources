# Compare resources in an AWS region

## Dependencies
- `aws`: must be configured, or use AWS CloudShell
- `jq`: `$ sudo apt-get install -y jq # in ubuntu` (already in the CloudShell)
- `aws-list-all`: `$ pip3 install --user aws-list-all` (needs python3 and python3-pip, but they exist in CloudShell)
- use `$ sudo pip3 install aws-list-all` if last one fails

#### Note:
- __aws-list-all__ needs __resource__ python module which is available only in Linux environments, where it is installed automatically. 
- You cannot use `compare.sh` in Windows, but it may work on MacOS.

## Setup & Run
```

$ git clone https://alexandrucpetrini@bitbucket.org/alexandrucpetrini/aws-compare-resources.git
$ cd aws-compare-resources
$ ./compare.sh
```
## Commands
* __./compare.sh__ 
* __./compare.sh__  __-f__, __- -force__, __- -force-init__ delete the lock to overwrite base json files
