#! /bin/bash
LOCKFILE_NAME="base_state"
LOCKFILE="${LOCKFILE_NAME}.lock"
BASE_STATE_FOLDER="base_state"
CURRENT_STATE_FOLDER="current_state"
REGION="us-east-2"
COMMAND_BASE_STATE="aws-list-all query --region ${REGION} --directory ./${BASE_STATE_FOLDER}/ --parallel 1"
COMMAND_CURRENT_STATE="aws-list-all query --region ${REGION} --directory ./${CURRENT_STATE_FOLDER}/ --parallel 1"
COMMAND_DIFF_AWS_SHOW="diff <(aws-list-all show ./${BASE_STATE_FOLDER}/*) <(aws-list-all show ./${CURRENT_STATE_FOLDER}/*)"

BASE_STATE_DIFF_FOLDER="base_state_diff"
CURRENT_STATE_DIFF_FOLDER="current_state_diff"

# DIFF_LINES_IGNORED="-I RequestId"
DIFF_ITEMS_IGNORED="--ignore-file-name-case --ignore-case --ignore-all-space --ignore-blank-lines"
DIFF_SIDE_BY_SIDE="--side-by-side --suppress-common-lines"
# DIFF_CONTEXTUAL="-c"
GREP_PIPE_CMD="| grep -E -v '\{|\}' | grep -E -i -v 'requestid|date|content-length|x-'"

# COMMAND_DIFF_ON_DIFF_DIRS="diff ${DIFF_SIDE_BY_SIDE} ${DIFF_ITEMS_IGNORED} ${DIFF_LINES_IGNORED} ${BASE_STATE_DIFF_FOLDER}/ ${CURRENT_STATE_DIFF_FOLDER}/  $GREP_PIPE_CMD"
COMMAND_DIFF_ON_DIRS="diff ${DIFF_CONTEXTUAL} ${DIFF_SIDE_BY_SIDE} ${DIFF_ITEMS_IGNORED} ${DIFF_LINES_IGNORED} ${BASE_STATE_FOLDER}/ ${CURRENT_STATE_FOLDER}/ $GREP_PIPE_CMD"

lock_base_state() {
    echo "Adding base state lockfile.."
    touch ./$LOCKFILE
}

format_json_files() {
  echo "Formatting json files:"
  for file in ./$1/*.json; do 
    echo "Processing $file.."
    formatted_file="$(jq -S '.' $file)" 
    echo "$formatted_file" > $file
  done
}

backup_base_state() {
  if [ ! -d $BASE_STATE_FOLDER ]; then return 0; fi
  echo "Backing Up"
  date_with_time=`date "+%Y%m%d-%H%M%S-%3N"`
  cp -rf $BASE_STATE_FOLDER "${BASE_STATE_FOLDER}-${date_with_time}"
}

if [ "$1" = "--force-init" -o "$1" = "-f"  -o "$1" = "--force" ]; then
  echo "Are you sure you want to reset the base state?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) 
        rm -f *.lock
        # backup_base_state 
        # rm -rf $BASE_STATE_FOLDER`
        rm -rf $CURRENT_STATE_FOLDER
        break
      ;;
      No ) 
        break
      ;;
    esac
  done
fi

if [ ! -e $LOCKFILE ] || [ ! -d $BASE_STATE_FOLDER ]; then
  echo "Do you wish to re-initialize the AWS services base state?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) 
        backup_base_state
        rm -rf $BASE_STATE_FOLDER
        ! $COMMAND_BASE_STATE 
        format_json_files $BASE_STATE_FOLDER
        lock_base_state
        break
        ;;
      No ) 
        if [ -d $BASE_STATE_FOLDER ]; then
          lock_base_state
        fi
        break
        ;;
    esac
  done
fi

if [ ! -d $BASE_STATE_FOLDER ]; then
  if [ -e $LOCKFILE ]; then
    echo "No base state. Removing lockfile." 
    rm -f $LOCKFILE
  fi
  echo "Please re-run the script to initialize!"
  exit 1
fi

echo "Getting current state:"
! $COMMAND_CURRENT_STATE
format_json_files $CURRENT_STATE_FOLDER

diff_show="$(echo $COMMAND_DIFF_AWS_SHOW | bash)"

# echo "--------------------------------------------------------------------------------"
echo -e "Changes:"
echo "--------------------------------------------------------------------------------"
if [ -z "$diff_show" ]; then
  echo "None."
  exit 0
fi
echo -e "$diff_show\n"

# this does not work well
# compare_diff_folders() {
#   while IFS= read -r line; do
#     # echo "$line"
#     [ -d $BASE_STATE_DIFF_FOLDER ] && rm -rf $BASE_STATE_DIFF_FOLDER || true
#     [ -d $CURRENT_STATE_DIFF_FOLDER ] && rm -rf $CURRENT_STATE_DIFF_FOLDER || true
#     mkdir ./$BASE_STATE_DIFF_FOLDER
#     mkdir ./$CURRENT_STATE_DIFF_FOLDER
#     line=(${line// / })

#     # if [ "<" = "${line[0]}" ]; then
#     #   cp -fv "./${BASE_STATE_FOLDER}/${line[1]}"* $BASE_STATE_DIFF_FOLDER
#     # fi

#     # if [ "${line[0]}" = '<' ]; then
#     #   cp -fv "./${CURRENT_STATE_FOLDER}/${line[1]}"* $CURRENT_STATE_DIFF_FOLDER
#     # fi

#     if [ -"n ${line[1]}" ]; then
#       cp -f "./${BASE_STATE_FOLDER}/${line[1]}"* $BASE_STATE_DIFF_FOLDER
#       cp -f "./${CURRENT_STATE_FOLDER}/${line[1]}"* $CURRENT_STATE_DIFF_FOLDER
#     fi
#   done < <(printf '%s\n' "$diff_show")

#   # format_json_files $BASE_STATE_DIFF_FOLDER
#   # format_json_files $CURRENT_STATE_DIFF_FOLDER
#   echo "Do you want to see detailed changes?"
#   select yn in "Yes" "No"; do
#     case $yn in
#       Yes ) 
#         sh -c "$COMMAND_DIFF_ON_DIFF_DIRS" | less
#         break
#         ;;
#       No ) 
#         exit 0
#         break
#         ;;
#     esac
#   done
# }

compare_diffs () {
  echo "Do you want to see detailed changes?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) 
        eval $COMMAND_DIFF_ON_DIRS  | less
        break
        ;;
      No ) 
        exit 0
        break
        ;;
    esac
  done
}

compare_diffs

# compare_diff_folders